package org.binar.chl2.view;

import org.binar.chl2.service.FileHandle;
import org.binar.chl2.service.IFileHandle;

public class MainMenu extends Menu{

  @Override
  public void body() {
    System.out.println("Show body");
  }

  @Override
  public void selectOption() {
    System.out.println("0. Exit");
    IFileHandle fileHandle = new FileHandle();
    fileHandle.generateFile();
  }
}
