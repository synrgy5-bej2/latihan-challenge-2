package org.binar.chl2.service;

public interface IFileHandle {

  void readFile();
  void generateFile();

}
