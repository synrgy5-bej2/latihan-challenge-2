package org.binar.chl2;

import org.binar.chl2.service.FileHandleDocx;
import org.binar.chl2.service.IFileHandle;
import org.binar.chl2.view.MainMenu;
import org.binar.chl2.view.Menu;

public class Challenge2 {

  public static void main(String[] args) {
    Menu menu = new MainMenu();
    menu.showMenu();
  }

}
